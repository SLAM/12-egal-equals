import java.util.ArrayList;

class Truc {
    private final String nom;

    Truc(String unNom) {
        this.nom = unNom;
    }

    public static void main(String[] args) {
        Truc t1 = new Truc("tux");
        Truc t2 = new Truc("tux");

        System.out.println(t1 == t2);       // false
        System.out.println(t1.equals(t2));  // false

        ArrayList<Truc> lesTrucs = new ArrayList<Truc>();
        lesTrucs.add(t1);
        System.out.println(lesTrucs.contains(t1));  // true
        System.out.println(lesTrucs.contains(t2));  // false
    }
}
