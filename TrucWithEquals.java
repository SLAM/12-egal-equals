import java.util.ArrayList;

class TrucWithEquals {
    private final String nom;

    TrucWithEquals(String unNom) {
        this.nom = unNom;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TrucWithEquals other = (TrucWithEquals) obj;
        return this.nom.equals(other.nom);
    }

    public static void main(String[] args) {
        TrucWithEquals t1 = new TrucWithEquals("tux");
        TrucWithEquals t2 = new TrucWithEquals("tux");

        System.out.println(t1 == t2);       // false
        System.out.println(t1.equals(t2));  // true

        ArrayList<TrucWithEquals> lesTrucs = new ArrayList<TrucWithEquals>();
        lesTrucs.add(t1);
        System.out.println(lesTrucs.contains(t1));  // true
        System.out.println(lesTrucs.contains(t2));  // true
    }
}
